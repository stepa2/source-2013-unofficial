//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose:
//
//=============================================================================

#ifndef CSACHIEVEMENTSANDSTATSINTERFACE_H
#define CSACHIEVEMENTSANDSTATSINTERFACE_H
#ifdef _WIN32
#pragma once
#endif

#include "achievements_and_stats_interface.h"
#include "lib_valve/vgui_controls/Panel.h"
#include "lib_valve/vgui_controls/PHandle.h"
#include "lib_valve/vgui_controls/MenuItem.h"
#include "lib_valve/vgui_controls/MessageDialog.h"

#include "cs_gamestats_shared.h"
#include "../client/cstrike/VGUI/achievement_stats_summary.h"
#include "lib_valve/vgui/IInput.h"
#include "lib_valve/vgui/ILocalize.h"
#include "lib_valve/vgui/IPanel.h"
#include "lib_valve/vgui/ISurface.h"
#include "lib_valve/vgui/ISystem.h"
#include "lib_valve/vgui/IVGui.h"


#if defined(CSTRIKE_DLL) && defined(CLIENT_DLL)

class CSAchievementsAndStatsInterface : public AchievementsAndStatsInterface
{
public:
    CSAchievementsAndStatsInterface();

    virtual void CreatePanel( vgui::Panel* pParent );
    virtual void DisplayPanel();
    virtual void ReleasePanel();
	virtual int GetAchievementsPanelMinWidth( void ) const { return cAchievementsDialogMinWidth; }

protected:
    vgui::DHANDLE<vgui::Frame>  m_pAchievementAndStatsSummary;
};

#endif

#endif // CSACHIEVEMENTSANDSTATSINTERFACE_H
