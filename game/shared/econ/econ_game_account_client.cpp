//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Code for the CEconGameAccountClient object
//
// $NoKeywords: $
//=============================================================================//

#include "base/cbase.h"

using namespace GCSDK;

#ifdef GC
IMPLEMENT_CLASS_MEMPOOL( CEconGameAccountClient, 10 * 1000, UTLMEMORYPOOL_GROW_SLOW );
#endif

// memdbgon must be the last include file in a .cpp file!!!
#include "lib_valve/tier0/memdbgon.h"

