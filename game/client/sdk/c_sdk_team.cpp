//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Client side C_SDKTeam class
//
// $NoKeywords: $
//=============================================================================//
#include "base/cbase.h"
#include "engine_systems/engine/IEngineSound.h"
#include "hud.h"
#include "recvproxy.h"
#include "c_sdk_team.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "lib_valve/tier0/memdbgon.h"


IMPLEMENT_CLIENTCLASS_DT(C_SDKTeam, DT_SDKTeam, CSDKTeam)
END_RECV_TABLE()

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
C_SDKTeam::C_SDKTeam()
{
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
C_SDKTeam::~C_SDKTeam()
{
}

