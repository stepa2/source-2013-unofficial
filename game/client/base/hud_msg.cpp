//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//
//
//  hud_msg.cpp
//
#include "base/cbase.h"
#include "clientmode.h"
#include "hudelement.h"
#include "lib_valve/tier1/KeyValues.h"
#include "lib_valve/vgui_controls/AnimationController.h"
#include "engine_systems/engine/IEngineSound.h"
#include "lib_valve/tier1/bitbuf.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "lib_valve/tier0/memdbgon.h"

/// USER-DEFINED SERVER MESSAGE HANDLERS

void CHud::MsgFunc_ResetHUD( bf_read &msg )
{
	ResetHUD();
}

void CHud::ResetHUD()
{
	// clear all hud data
	g_pClientMode->GetViewportAnimationController()->CancelAllAnimations();

	for ( int i = 0; i < m_HudList.Size(); i++ )
	{
		m_HudList[i]->Reset();
	}

	g_pClientMode->GetViewportAnimationController()->RunAllAnimationsToCompletion();
#ifndef _XBOX
	// reset sensitivity
	m_flMouseSensitivity = 0;
	m_flMouseSensitivityFactor = 0;
#endif
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------

void CHud::MsgFunc_SendAudio( bf_read &msg )
{
	char szString[2048];
	msg.ReadString( szString, sizeof(szString) );
	
	CLocalPlayerFilter filter;
	C_BaseEntity::EmitSound( filter, SOUND_FROM_LOCAL_PLAYER, szString );
}
