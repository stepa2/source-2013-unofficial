//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose:
//
//=============================================================================//

#ifndef TF_LOBBY_CONTAINER_FRAME_COMP_H
#define TF_LOBBY_CONTAINER_FRAME_COMP_H


#include "base/cbase.h"
//#include "tf_pvelobbypanel.h"
#include "game/client/iviewport.h"
#include "tf_shareddefs.h"
#include "econ/confirm_dialog.h"
#include "econ/econ_controls.h"
#include "engine_systems/engine/ienginevgui.h"
#include "tf_gc_client.h"
#include "tf_party.h"
#include "tf_item_inventory.h"
#include "econ_ui.h"

#include "lib_valve/vgui_controls/Tooltip.h"
#include "lib_valve/vgui_controls/PropertyDialog.h"
#include "lib_valve/vgui_controls/PropertySheet.h"
#include "lib_valve/vgui_controls/ComboBox.h"
#include "lib_valve/vgui_controls/RadioButton.h"
#include "lib_valve/vgui_controls/SectionedListPanel.h"
#include "lib_valve/vgui_controls/ScrollableEditablePanel.h"
#include "vgui_bitmapimage.h"
#include "lib_valve/vgui/IInput.h"
#include "lib_valve/vgui_controls/ImageList.h"
#include "lib_valve/vgui/IVGui.h"
#include "GameEventListener.h"
#include "vgui_avatarimage.h"
#include "lib_valve/vgui/ISurface.h"
#include "lib_valve/VGuiMatSurface/IMatSystemSurface.h"
#include "rtime.h"
#include "econ_game_account_client.h"
#include "tf_leaderboardpanel.h"
#include "tf_mapinfo.h"
#include "tf_ladder_data.h"
#include "tf_gamerules.h"
#include "confirm_dialog.h"
#include "tf_lobby_container_frame.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "lib_valve/tier0/memdbgon.h"

class CBaseLobbyPanel;

// This is a big fat kludge so I can use the PropertyPage
class CLobbyContainerFrame_Comp : public CBaseLobbyContainerFrame
{
	DECLARE_CLASS_SIMPLE( CLobbyContainerFrame_Comp, CBaseLobbyContainerFrame );
public:
	CLobbyContainerFrame_Comp();
	~CLobbyContainerFrame_Comp();

	//
	// PropertyDialog overrides
	//
	virtual void ShowPanel( bool bShow ) OVERRIDE;
	virtual void OnCommand( const char *command ) OVERRIDE;

protected:

	virtual void WriteControls();

private:
	virtual const char* GetResFile() const OVERRIDE { return "Resource/UI/LobbyContainerFrame_Comp.res"; }
	virtual TF_MatchmakingMode GetHandledMode() const { return TF_Matchmaking_LADDER; }
	virtual bool VerifyPartyAuthorization() const;
	virtual void HandleBackPressed() OVERRIDE;
};

#endif //TF_LOBBY_CONTAINER_FRAME_COMP_H