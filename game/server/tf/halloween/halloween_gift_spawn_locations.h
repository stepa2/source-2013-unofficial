//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

// memdbgon must be the last include file in a .cpp file!!!
#include "base/cbase.h"
#include "lib_valve/tier1/utlvector.h"
#include "util.h"
#include "lib_valve/tier0/memdbgon.h"

//
void AddHalloweenGiftPositionsForMap( const char *pszMapName, CUtlVector<Vector> &vLocations );
//
