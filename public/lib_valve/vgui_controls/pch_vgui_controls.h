//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

#ifndef PCH_VGUI_CONTROLS_H
#define PCH_VGUI_CONTROLS_H

#ifdef _WIN32
#pragma once
#endif

// general includes
#include <ctype.h>
#include <stdlib.h>
#include "lib_valve/tier0/dbg.h"
#include "lib_valve/tier0/valve_off.h"
#include "lib_valve/tier1/KeyValues.h"

#include "lib_valve/tier0/valve_on.h"
#include "lib_valve/tier0/memdbgon.h"

#include "filesystem.h"
#include "lib_valve/tier0/validator.h"

// vgui includes
#include "lib_valve/vgui/IBorder.h"
#include "lib_valve/vgui/IInput.h"
#include "lib_valve/vgui/ILocalize.h"
#include "lib_valve/vgui/IPanel.h"
#include "lib_valve/vgui/IScheme.h"
#include "lib_valve/vgui/ISurface.h"
#include "lib_valve/vgui/ISystem.h"
#include "lib_valve/vgui/IVGui.h"
#include "lib_valve/vgui/KeyCode.h"
#include "lib_valve/vgui/Cursor.h"
#include "lib_valve/vgui/MouseCode.h"

// vgui controls includes
#include "lib_valve/vgui_controls/Controls.h"

#include "lib_valve/vgui_controls/AnimatingImagePanel.h"
#include "lib_valve/vgui_controls/AnimationController.h"
#include "lib_valve/vgui_controls/BitmapImagePanel.h"
#include "lib_valve/vgui_controls/BuildGroup.h"
#include "lib_valve/vgui_controls/BuildModeDialog.h"
#include "lib_valve/vgui_controls/Button.h"
#include "lib_valve/vgui_controls/CheckButton.h"
#include "lib_valve/vgui_controls/CheckButtonList.h"
#include "lib_valve/vgui_controls/ComboBox.h"
#include "lib_valve/vgui_controls/Controls.h"
#include "lib_valve/vgui_controls/DialogManager.h"
#include "lib_valve/vgui_controls/DirectorySelectDialog.h"
#include "lib_valve/vgui_controls/Divider.h"
#include "lib_valve/vgui_controls/EditablePanel.h"
#include "lib_valve/vgui_controls/FileOpenDialog.h"
#include "lib_valve/vgui_controls/FocusNavGroup.h"
#include "lib_valve/vgui_controls/Frame.h"
#include "lib_valve/vgui_controls/GraphPanel.h"
#include "lib_valve/vgui_controls/HTML.h"
#include "lib_valve/vgui_controls/Image.h"
#include "lib_valve/vgui_controls/ImageList.h"
#include "lib_valve/vgui_controls/ImagePanel.h"
#include "lib_valve/vgui_controls/Label.h"
#include "lib_valve/vgui_controls/ListPanel.h"
#include "lib_valve/vgui_controls/ListViewPanel.h"
#include "lib_valve/vgui_controls/Menu.h"
#include "lib_valve/vgui_controls/MenuBar.h"
#include "lib_valve/vgui_controls/MenuButton.h"
#include "lib_valve/vgui_controls/MenuItem.h"
#include "lib_valve/vgui_controls/MessageBox.h"
#include "lib_valve/vgui_controls/Panel.h"
#ifndef HL1
#include "lib_valve/vgui_controls/PanelAnimationVar.h"
#endif
#include "lib_valve/vgui_controls/PanelListPanel.h"
#include "lib_valve/vgui_controls/PHandle.h"
#include "lib_valve/vgui_controls/ProgressBar.h"
#include "lib_valve/vgui_controls/ProgressBox.h"
#include "lib_valve/vgui_controls/PropertyDialog.h"
#include "lib_valve/vgui_controls/PropertyPage.h"
#include "lib_valve/vgui_controls/PropertySheet.h"
#include "lib_valve/vgui_controls/QueryBox.h"
#include "lib_valve/vgui_controls/RadioButton.h"
#include "lib_valve/vgui_controls/RichText.h"
#include "lib_valve/vgui_controls/ScrollBar.h"
#include "lib_valve/vgui_controls/ScrollBarSlider.h"
#include "lib_valve/vgui_controls/SectionedListPanel.h"
#include "lib_valve/vgui_controls/Slider.h"
#ifndef HL1
#include "lib_valve/vgui_controls/Splitter.h"
#endif
#include "lib_valve/vgui_controls/TextEntry.h"
#include "lib_valve/vgui_controls/TextImage.h"
#include "lib_valve/vgui_controls/ToggleButton.h"
#include "lib_valve/vgui_controls/Tooltip.h"
#ifndef HL1
#include "lib_valve/vgui_controls/ToolWindow.h"
#endif
#include "lib_valve/vgui_controls/TreeView.h"
#ifndef HL1
#include "lib_valve/vgui_controls/TreeViewListControl.h"
#endif
#include "lib_valve/vgui_controls/URLLabel.h"
#include "lib_valve/vgui_controls/WizardPanel.h"
#include "lib_valve/vgui_controls/WizardSubPanel.h"

#ifndef HL1
#include "lib_valve/vgui_controls/KeyBoardEditorDialog.h"
#include "lib_valve/vgui_controls/InputDialog.h"
#endif

#endif // PCH_VGUI_CONTROLS_H