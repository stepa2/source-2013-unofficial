//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================

#ifndef MOVIEOBJECTS_H
#define MOVIEOBJECTS_H
#ifdef _WIN32
#pragma once
#endif

#include "lib_valve/movieobjects/dmeclip.h"
#include "lib_valve/movieobjects/dmetransform.h"
#include "lib_valve/movieobjects/dmetransformlist.h"
#include "lib_valve/movieobjects/dmecamera.h"
#include "lib_valve/movieobjects/dmeshape.h"
#include "lib_valve/movieobjects/dmevertexdata.h"
#include "lib_valve/movieobjects/dmemesh.h"
#include "lib_valve/movieobjects/dmefaceset.h"
#include "lib_valve/movieobjects/dmematerial.h"
#include "lib_valve/movieobjects/dmetimeframe.h"
#include "lib_valve/movieobjects/dmedag.h"
#include "lib_valve/movieobjects/dmelight.h"
#include "lib_valve/movieobjects/dmegamemodel.h"
#include "lib_valve/movieobjects/dmemdl.h"
#include "lib_valve/movieobjects/dmesound.h"
#include "lib_valve/movieobjects/dmeoperator.h"
#include "lib_valve/movieobjects/dmeanimationlist.h"
#include "lib_valve/movieobjects/dmeattachment.h"
#include "lib_valve/movieobjects/dmejoint.h"
#include "lib_valve/movieobjects/dmemorphoperator.h"
#include "lib_valve/movieobjects/dmecombinationoperator.h"
#include "lib_valve/movieobjects/dmetransformoperator.h"
#include "lib_valve/movieobjects/dmepackoperators.h"
#include "lib_valve/movieobjects/dmeunpackoperators.h"
#include "lib_valve/movieobjects/dmeexpressionoperator.h"
#include "lib_valve/movieobjects/dmeinput.h"
#include "lib_valve/movieobjects/dmegamemodelinput.h"
#include "lib_valve/movieobjects/dmekeyboardinput.h"
#include "lib_valve/movieobjects/dmemouseinput.h"
#include "lib_valve/movieobjects/dmechannel.h"
#include "lib_valve/movieobjects/dmelog.h"
#include "lib_valve/movieobjects/dmetrack.h"
#include "lib_valve/movieobjects/dmetrackgroup.h"
#include "lib_valve/movieobjects/dmeanimationset.h"
#include "lib_valve/movieobjects/dmebalancetostereocalculatoroperator.h"
#include "lib_valve/movieobjects/dmemodel.h"
#include "lib_valve/movieobjects/dmemakefile.h"
#include "lib_valve/movieobjects/dmedccmakefile.h"
#include "lib_valve/movieobjects/dmemdlmakefile.h"
#include "lib_valve/movieobjects/dmeeditortypedictionary.h"
#include "lib_valve/movieobjects/dmephonememapping.h"
#include "lib_valve/movieobjects/dmetimeselection.h"
#include "lib_valve/movieobjects/dmeselection.h"
#include "lib_valve/movieobjects/dmeeyeposition.h"
#include "lib_valve/movieobjects/dmeeyeball.h"
#include "lib_valve/movieobjects/dmedrawsettings.h"

#define USING_ELEMENT_FACTORY( className )			\
	extern C##className *g_##C##className##LinkerHack;		\
	C##className *g_##C##className##PullInModule = g_##C##className##LinkerHack;

#endif // MOVIEOBJECTS_H
