//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

#ifndef GC_SCHEMA_H
#define GC_SCHEMA_H
#ifdef _WIN32
#pragma once
#endif

#include "lib_valve/tier0/platform.h"
#include "steam/steamtypes.h"

#include "lib_valve/tier0/dbg.h"

// include this before checksum_crc specifically to avoid the 
// CRC references
#include "steam/steamclientpublic.h"

#include "lib_valve/tier1/utlmap.h"
#include "lib_valve/tier1/utlstring.h"
#include "lib_valve/tier1/utlbuffer.h"
#include "lib_valve/tier1/mempool.h"
#include "lib_valve/tier1/tsmempool.h"
#include "lib_valve/tier1/tsmultimempool.h"
#include "lib_valve/tier1/fmtstr.h"

#include "lib_valve/vstdlib/coroutine.h"

// public stuff
#include "gamecoordinator/igcsqlresultsetlist.h"

// These are first since they're used all over
#include "gcconstants.h"
#include "lib_valve/tier1/refcount.h"

// SQL Access stuff
#include "sqlaccess/record.h"
#include "sqlaccess/schema.h"

#include "sqlaccess/recordinfo.h"
#include "sqlaccess/schemafull.h"




#endif