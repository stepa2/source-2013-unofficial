//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: includes all the headers required for the GC SDK. Include this
//			in your stdafx.h
//
//=============================================================================

#ifndef GCSDK_H
#define GCSDK_H
#ifdef _WIN32
#pragma once
#endif

#if defined(_WIN32) || defined(_WIN64)
#pragma once
#include <intrin.h>
#pragma intrinsic(_BitScanReverse)
#endif

#include "lib_valve/tier0/platform.h"

#include "gcsteamdefines.h"

#include "steam/steamtypes.h"

#include "lib_valve/tier0/dbg.h"
#ifdef DBGFLAG_VALIDATE
#include "lib_valve/tier0/validator.h"
#endif
#include "lib_valve/tier0/vprof.h"
#include "lib_valve/tier0/fasttimer.h"

// include this before checksum_crc specifically to avoid the 
// CRC references
#include "steam/steamclientpublic.h"

#include "lib_valve/tier1/utlmap.h"
#include "lib_valve/tier1/utllinkedlist.h"
#include "lib_valve/tier1/utlpriorityqueue.h"
#include "lib_valve/tier1/utlstring.h"
#include "lib_valve/tier1/utlbuffer.h"
#include "lib_valve/tier1/utldict.h"
#include "lib_valve/tier1/utlhashmaplarge.h"
#include "lib_valve/tier1/mempool.h"
#include "lib_valve/tier1/tsmempool.h"
#include "lib_valve/tier1/tsmultimempool.h"
#include "lib_valve/tier1/checksum_crc.h"
#include "lib_valve/tier1/fmtstr.h"
#include "lib_valve/tier1/KeyValues.h"
#include "lib_valve/tier1/strtools.h"
#include "lib_valve/tier1/utlsymbol.h"
#include "lib_valve/tier1/utlsymbollarge.h"

#include "lib_valve/vstdlib/coroutine.h"
#include "lib_valve/vstdlib/osversion.h"

// public stuff
#include "gamecoordinator/igcsqlresultsetlist.h"
#include "misc.h"

// These are first since they're used all over
#include "gcconstants.h"
#include "lib_valve/tier1/refcount.h"
#include "netpacket.h"
#include "gcmsg.h"
#include "msgprotobuf.h"
#include "gc_convar.h"

// SQL Access stuff
#include "sqlaccess/record.h"
#include "sqlaccess/schema.h"
#include "sqlaccess/recordinfo.h"
#include "sqlaccess/schemafull.h"
#include "sqlaccess/columnset.h"
#include "sqlaccess/sqlrecord.h"
#include "sqlaccess/sqlutil.h"
#include "sqlaccess/sqlaccess.h"

#include "messagelist.h"
#include "gchost.h"
#include "gclogger.h"
#include "gcsqlquery.h"
#include "jobtime.h"
#include "job.h"
#include "jobmgr.h"
#include "netpacketpool.h"
#include "gcsystemmsgs.h"
#include "gcwgjobmgr.h"
#include "gcbase.h"
#include "gcsession.h"
#include "sharedobject.h"
#include "protobufsharedobject.h"
#include "schemasharedobject.h"
#include "sharedobjectcache.h"
#include "gcdirtyfield.h"
#include "gc_sharedobjectcache.h"
#include "http.h"
#include "gcwebapi.h"
#include "gcwebapikey.h"
#include "webapi_response.h"
#include "gcjob.h"
#include "msgprotobuf.h"
#include "sdocache.h"

#endif // GCSDK_H
