//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: includes all the headers required for the cliend side of the GC 
//			SDK GC SDK. Include this in your stdafx.h
//
//=============================================================================

#ifndef GCCLIENTSDK_H
#define GCCLIENTSDK_H
#ifdef _WIN32
#pragma once
#endif

#include "gcsteamdefines.h"
#include "lib_valve/tier0/platform.h"
#include "steam/steamtypes.h"

#include "lib_valve/tier0/dbg.h"
#include "lib_valve/tier0/vprof.h"
#include "lib_valve/tier0/fasttimer.h"
#include "lib_valve/tier0/t0constants.h"

#include "lib_valve/tier1/utlmap.h"
#include "lib_valve/tier1/utllinkedlist.h"
#include "lib_valve/tier1/utlpriorityqueue.h"
#include "lib_valve/tier1/utlstring.h"
#include "lib_valve/tier1/utlbuffer.h"
#include "lib_valve/tier1/mempool.h"
#include "lib_valve/tier1/tsmempool.h"
#include "lib_valve/tier1/tsmultimempool.h"
#include "lib_valve/tier1/checksum_crc.h"
#include "lib_valve/tier1/fmtstr.h"

#include "lib_valve/vstdlib/coroutine.h"

// public stuff
#include "steam/steamclientpublic.h"
#include "misc.h"
#include "steam/isteamclient.h"
#include "steam/isteamgamecoordinator.h"
#include "steam/steam_api.h"

// stuff to include early because it is widely depended on
#include "netpacket.h"
#include "gcmsg.h"
#include "msgprotobuf.h"
#include "gcconstants.h"
#include "lib_valve/tier1/refcount.h"

#include "jobtime.h"
#include "messagelist.h"
#include "gclogger.h"
#include "job.h"
#include "jobmgr.h"
#include "netpacketpool.h"
#include "sharedobject.h"
#include "protobufsharedobject.h"
#include "sharedobjectcache.h"
#include "gcclient_sharedobjectcache.h"
#include "gcclient.h"
#include "gcclientjob.h"
#include "gcsystemmsgs.h"

#include "webapi_response.h"

#endif // GCCLIENTSDK_H
