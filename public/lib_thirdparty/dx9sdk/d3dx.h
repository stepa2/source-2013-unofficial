///////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 1999 Microsoft Corporation.  All Rights Reserved.
//
//  File:       d3dx.h
//  Content:    D3DX utility library
//
///////////////////////////////////////////////////////////////////////////

#ifndef __D3DX_H__
#define __D3DX_H__

#ifndef D3DXINLINE
#ifdef __cplusplus
#define D3DXINLINE inline
#else
#define D3DXINLINE _inline
#endif
#endif

#include "lib_thirdparty/dx9sdk/d3dxcore.h"
#include "lib_thirdparty/dx9sdk/d3dxmath.h"
#include "lib_thirdparty/dx9sdk/d3dxshapes.h"
#include "lib_thirdparty/dx9sdk/d3dxsprite.h"

#endif //__D3DX_H__
