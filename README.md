Source 2013 Unofficial

# Build instructions

### Prerequisites
1. Install Source SDK Base 2013 Singleplayer
2. Clone this repository to subfolder of Source SDK Installation folder
  (e.g. to D:\Source-2013\!SRC for D:\Source-2013)

### For Windows/Visual Studio 
1. Build /vpc/vpc.sln solution
2. Run /engine_systems/stdshaders/buildallshaders.bat
3. Run /vpc_everything.bat
4. Build /everything.sln


# Инструкции по сборке

### Предварительные требования
1. Установите Source SDK Base 2013 Singleplayer
2. Клонируйте данный репозиторий в подпапку папки установки Source SDK
  (к примеру, в D:\Source-2013\!SRC , если SDK установлен в D:\Source-2013)

### Сборка в Windows/Visual Studio
1. Соберите решение /vpc/vpc.sln
2. Запустите /engine_systems/stdshaders/buildallshaders.bat
3. Запустите /vpc_everything.bat
4. Соберите решение /everything.sln
