//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose:
//
//=============================================================================

#ifndef PCH_MATERIALSYSTEM_H
#define PCH_MATERIALSYSTEM_H

#if defined( _WIN32 )
#pragma once
#endif

#if defined( _WIN32 ) && !defined( _X360 )
#define WIN32_LEAN_AND_MEAN 1
#include "windows.h"
#endif

#include <malloc.h>
#include <string.h>
#include "shared/crtmemdebug.h"

#include "lib_valve/tier0/platform.h"
#include "lib_valve/tier0/dbg.h"
#include "lib_valve/tier0/fasttimer.h"
#include "lib_valve/tier0/vprof.h"

#include "lib_valve/tier1/tier1.h"
#include "lib_valve/tier1/utlstack.h"
#include "lib_valve/tier1/generichash.h"
#include "lib_valve/tier1/utlsymbol.h"
#include "lib_valve/tier1/utlrbtree.h"
#include "lib_valve/tier1/strtools.h"
#include "lib_valve/tier0/icommandline.h"
#include "lib_valve/mathlib/vmatrix.h"
#include "icvar.h"
#include "lib_valve/tier1/KeyValues.h"
#include "lib_valve/tier1/convar.h"

#include "lib_valve/tier2/tier2.h"
#include "lib_valve/bitmap/imageformat.h"
#include "lib_valve/bitmap/tgawriter.h"
#include "lib_valve/bitmap/tgaloader.h"
#include "lib_valve/datacache/idatacache.h"
#include "filesystem.h"
#include "pixelwriter.h"

#include "materialsystem_global.h"
#include "engine_systems/materialsystem/imaterialvar.h"
#include "engine_systems/materialsystem/imesh.h"
#include "engine_systems/materialsystem/IColorCorrection.h"

#include "imaterialinternal.h"
#include "imaterialsysteminternal.h"

#endif // PCH_MATERIALSYSTEM_H
