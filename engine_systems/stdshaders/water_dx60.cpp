//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

#include "engine_systems/shaderlib/cshader.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "lib_valve/tier0/memdbgon.h"

// NOTE: Water DX60 is located in LightmappedGeneric_DX6 so it can inherit
DEFINE_FALLBACK_SHADER( Water, Water_DX60 )

