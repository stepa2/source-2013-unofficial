@echo off
setlocal

echo ~~~~~~ buildallshaders.bat %* ~~~~~~

set sourcedir="shaders"
set targetdir="..\..\..\shared\shaders"

set BUILD_SHADER=call buildshaders.bat

REM ****************
REM PC SHADERS
REM ****************

%BUILD_SHADER% stdshader_dx9_20b
%BUILD_SHADER% stdshader_dx9_20b_new	-dx9_30
%BUILD_SHADER% stdshader_dx9_30			-dx9_30	-force30
rem %BUILD_SHADER% stdshader_dx10     	-dx10

echo Finished dynamic buildallshaders.bat %*