//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Teeth renderer
//
// $Header: $
// $NoKeywords: $
//=============================================================================//
#include "engine_systems/shaderlib/cshader.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "lib_valve/tier0/memdbgon.h"

DEFINE_FALLBACK_SHADER( Sky, UnlitGeneric )
DEFINE_FALLBACK_SHADER( Sky_dx6, UnlitGeneric )

