@echo off

set TTEXE=..\..\devtools\bin\timeprecise.exe
if not exist %TTEXE% goto no_ttexe
goto no_ttexe_end

:no_ttexe
set TTEXE=time /t
:no_ttexe_end

echo.
rem echo ==================== buildshaders %* ==================
%TTEXE% -cur-Q
set tt_start=%ERRORLEVEL%
set tt_chkpt=%tt_start%


REM ****************
REM usage: buildshaders <shaderProjectName>
REM ****************

setlocal

if "%1" == "" (
	echo.
	echo "usage: buildshaders <shaderProjectName> [-dx10] [-game gameDir] [-source sourceDir]"
	echo "       gameDir is where gameinfo.txt is (where it will store the compiled shaders)."
	echo "       sourceDir is where the source code is (where it will find scripts and compilers)."
	echo "ex   : buildshaders myshaders"
	echo "ex   : buildshaders myshaders -game c:\steam\steamapps\sourcemods\mymod -source c:\mymod\src"
	goto :end
)
set inputbase=%1

set targetdir=..\..\..\shared\shaders
set SrcDirBase=..\..
set shaderDir=shaders
set SHADERINCPATH=vshtmp9/... fxctmp9/...


	set DIRECTX_SDK_VER=pc09.00
	set DIRECTX_SDK_BIN_DIR=dx9sdk\utilities

if /i "%2" == "-dx9_30" (
	set DIRECTX_SDK_VER=pc09.30
	set DIRECTX_SDK_BIN_DIR=dx10sdk\utilities\dx9_30
)
if /i "%2" == "-dx10" (
	set DIRECTX_SDK_VER=pc10.00
	set DIRECTX_SDK_BIN_DIR=dx10sdk\utilities\dx10_40
)

if /i "%3" == "-force30" (
	set DIRECTX_FORCE_MODEL=30
)

rem MOD ARGS - look for -game or the vproject environment variable
if /i "%2" == "-game" (
	if /i "%4" NEQ "-source" goto NoSourceDirSpecified
	set SrcDirBase=%~5

	REM ** use the -game parameter to tell us where to put the files
	set targetdir=%~3\shaders

	if not exist "%~3\gameinfo.txt" goto InvalidGameDirectory
)

goto BuildShaders

REM ****************
REM ERRORS
REM ****************
:InvalidGameDirectory
echo -
echo Error: "%~3" is not a valid game directory.
echo (The -game directory must have a gameinfo.txt file)
echo -
goto end

:NoSourceDirSpecified
echo ERROR: If you specify -game on the command line, you must specify -source.
goto usage
goto end

REM ****************
REM BUILD SHADERS
REM ****************

:BuildShaders


if not exist %shaderDir% mkdir %shaderDir%
if not exist %shaderDir%\fxc mkdir %shaderDir%\fxc
if not exist %shaderDir%\vsh mkdir %shaderDir%\vsh
if not exist %shaderDir%\psh mkdir %shaderDir%\psh

if exist filelist.txt del /f /q filelist.txt
if exist filestocopy.txt del /f /q filestocopy.txt
if exist filelistgen.txt del /f /q filelistgen.txt
if exist inclist.txt del /f /q inclist.txt
if exist vcslist.txt del /f /q vcslist.txt


REM ****************
REM Generate a makefile for the shader project
REM ****************
perl "%SrcDirBase%\devtools\bin\updateshaders.pl" -source "%SrcDirBase%" %inputbase%


REM ****************
REM Run the makefile, generating minimal work/build list for fxc files, go ahead and compile vsh and psh files.
REM ****************
echo Building inc files, asm vcs files, and VMPI worklist for %inputbase%...
%SrcDirBase%\devtools\bin\nmake /S /C -f makefile.%inputbase%

REM ****************
REM Copy the inc files to their target
REM ****************
if exist "inclist.txt" (
	echo Publishing shader inc files to target...
	perl %SrcDirBase%\devtools\bin\copyshaderincfiles.pl inclist.txt
)

REM ****************
REM END
REM ****************
:end


%TTEXE% -diff %tt_start%
echo.

