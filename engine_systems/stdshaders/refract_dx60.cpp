//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#include "engine_systems/shaderlib/cshader.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "lib_valve/tier0/memdbgon.h"

// FIXME: This is a placeholder. . need to do something for real here.
DEFINE_FALLBACK_SHADER( Refract, Refract_DX60 )
DEFINE_FALLBACK_SHADER( Refract_DX60, UnlitGeneric )


