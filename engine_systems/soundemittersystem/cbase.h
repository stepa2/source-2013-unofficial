//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#ifndef CBASE_H
#define CBASE_H
#ifdef _WIN32
#pragma once
#endif

// Shared header file
#include "lib_valve/tier0/basetypes.h"
#include "lib_valve/tier1/strtools.h"
#include "lib_valve/vstdlib/random.h"

#ifdef _WIN32
extern IUniformRandomStream *random;
#endif

#endif // CBASE_H
