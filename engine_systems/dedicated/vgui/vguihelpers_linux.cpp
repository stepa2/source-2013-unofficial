//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//
#include "lib_valve/tier1/interface.h"

bool InitializeVGui(CreateInterfaceFn *factorylist, int factorycount)
{
}

int StartVGUI()
{
}

void StopVGUI()
{
}

void RunVGUIFrame()
{
}

bool VGUIIsRunning()
{
}

bool VGUIIsStopping()
{
}

bool VGUIIsInConfig()
{
}

void VGUIFinishedConfig()
{
}

