//-----------------------------------------------------------------------------
//	SHADERAPIDX10.VPC
//
//	Project Script
//-----------------------------------------------------------------------------

$macro SRCDIR		"..\.."
$Macro OUTBINDIR	"$SRCDIR\..\bin"

$Include "$SRCDIR\vpc_scripts\source_dll_base.vpc"

// Common Configuration
$Configuration
{
	$Compiler
	{
		$PreprocessorDefinitions		"$BASE;SHADERAPIDX10;SHADER_DLL_EXPORT;PROTECTED_THINGS_ENABLE;strncpy=use_Q_strncpy_instead;_snprintf=use_Q_snprintf_instead"
		$PreprocessorDefinitions		"$BASE;USE_ACTUAL_DX" [$WIN32]

//		$AdditionalOptions		"/FC"
	}
}

$Project "shaderapidx10"
{
	$Folder	"Source Files"
	{
		// Shared riles
		$File	"..\shaderapidx9\cvballoctracker.cpp"
		$File	"..\shaderapidx9\shaderdevicebase.cpp"
		$File	"..\shaderapidx9\shaderapibase.cpp"
		$File	"..\shaderapidx9\meshbase.cpp"
		
		// DX10 related files
		$File	"ShaderDeviceDx10.cpp"		\
				"ShaderAPIDx10.cpp"			\
				"MeshDx10.cpp"				\
				"InputLayoutDx10.cpp"		\
				"ShaderShadowDx10.cpp"
  		{
			$Configuration
			{
				$Compiler
				{
					$PreprocessorDefinitions	"$BASE;DX10"
				}
			}
		}

		// DX9 related files	
		$File	"..\shaderapidx9\ColorFormatDX8.cpp"
		$File	"..\shaderapidx9\d3d_async.cpp"
		$File	"$SRCDIR\public\shared\filesystem_helpers.cpp"
		$File	"..\shaderapidx9\HardwareConfig.cpp"
		$File	"..\shaderapidx9\MeshDX8.cpp"
		$File	"..\shaderapidx9\Recording.cpp"
		$File	"..\shaderapidx9\ShaderAPIDX8.cpp"
		$File	"..\shaderapidx9\ShaderDeviceDX8.cpp"
		$File	"..\shaderapidx9\ShaderShadowDX8.cpp"
		$File	"..\shaderapidx9\TextureDX8.cpp"
		$File	"..\shaderapidx9\TransitionTable.cpp"
		$File	"..\shaderapidx9\vertexdecl.cpp"
		$File	"..\shaderapidx9\VertexShaderDX8.cpp"
		$File	"..\shaderapidx9\wmi.cpp"
	}

	$Folder	"DirectX Header Files"
	{
		$File	"$SRCDIR\public\lib_thirdparty\dx10sdk\d3dx10.h"
		$File	"$SRCDIR\public\lib_thirdparty\dx10sdk\d3dx10core.h"
		$File	"$SRCDIR\public\lib_thirdparty\dx10sdk\d3dx10math.h"
		$File	"$SRCDIR\public\lib_thirdparty\dx10sdk\d3dx10math.inl"
		$File	"$SRCDIR\public\lib_thirdparty\dx10sdk\d3dx10mesh.h"
		$File	"$SRCDIR\public\lib_thirdparty\dx10sdk\d3dx10tex.h"
	}

	$Folder	"Public Header Files"
	{
		$File	"$SRCDIR\public\engine_systems\shaderapi\ishaderdevice.h"
		$File	"$SRCDIR\public\engine_systems\shaderapi\ishaderutil.h"
		$File	"$SRCDIR\public\engine_systems\shaderapi\ishaderapi.h"
		$File	"$SRCDIR\public\engine_systems\shaderapi\ishaderdynamic.h"
		$File	"$SRCDIR\public\engine_systems\shaderapi\ishadershadow.h"
	}

	$Folder	"Header Files"
	{
		// Shared files
		$File	"..\shaderapidx9\meshbase.h"
		$File	"..\shaderapidx9\shaderdevicebase.h"
		$File	"..\shaderapidx9\shaderapibase.h"
		$File	"..\shaderapidx9\shaderapi_global.h"
		$File	"..\shaderapidx9\HardwareConfig.h"

		// DX10 related files
		$File	"ShaderDeviceDx10.h"
		$File	"ShaderAPIDx10.h"
		$File	"MeshDx10.h"
		$File	"ShaderShadowDx10.h"
		$File	"shaderapidx10_global.h"
		$File	"inputlayoutdx10.h"

		// DX9 related files		
		$File	"..\shaderapidx9\TransitionTable.h"
		$File	"..\shaderapidx9\vertexdecl.h"
		$File	"..\shaderapidx9\ColorFormatDX8.h"
		$File	"..\shaderapidx9\d3d_async.h"
		$File	"..\shaderapidx9\dynamicib.h"
		$File	"..\shaderapidx9\dynamicvb.h"
		$File	"..\shaderapidx9\IMeshDX8.h"
		$File	"..\shaderapidx9\locald3dtypes.h"
		$File	"..\shaderapidx9\Recording.h"
		$File	"..\shaderapidx9\ShaderAPIDX8.h"
		$File	"..\shaderapidx9\ShaderAPIDX8_Global.h"
		$File	"..\shaderapidx9\ShaderShadowDX8.h"
		$File	"..\shaderapidx9\stubd3ddevice.h"
		$File	"..\shaderapidx9\TextureDX8.h"
		$File	"..\shaderapidx9\VertexShaderDX8.h"
		$File	"..\shaderapidx9\wmi.h"
	}

	$Folder	"Link Libraries"
	{
		
		$Lib	"$LIBBUILD\tier2"
		$Lib	"$LIBBUILD\bitmap"
		$Lib	"$LIBBUILD\mathlib"
		$Lib	"$LIBBUILD\bzip2"
		
		$File	"$SRCDIR\lib_thirdparty\dx10sdk\x86\d3d9.lib" 
		$File	"$SRCDIR\lib_thirdparty\dx10sdk\x86\d3d10.lib" 
		$File	"$SRCDIR\lib_thirdparty\dx10sdk\x86\dxgi.lib" 
		
		$File	"$SRCDIR\lib_thirdparty\dx10sdk\x86\d3dx10.lib"	\
				"$SRCDIR\lib_thirdparty\dx10sdk\x86\d3dx9.lib"
		{
			$Configuration "Debug"
			{
				$ExcludedFromBuild	"Yes"
			}
		}
		
		$File	"$SRCDIR\lib_thirdparty\dx10sdk\x86\d3dx10d.lib"	\
				"$SRCDIR\lib_thirdparty\dx10sdk\x86\d3dx9d.lib"
		{
			$Configuration "Release"
			{
				$ExcludedFromBuild	"Yes"
			}
		}
	}
}
