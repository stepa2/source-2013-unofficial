//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//===========================================================================//
// Primary header for engine
#if !defined( QUAKEDEF_H )
#define QUAKEDEF_H
#ifdef _WIN32
#pragma once
#endif

// OPTIONAL DEFINES
//#define	PARANOID		// speed sapping error checking
#include "lib_valve/tier0/basetypes.h"
#include "qlimits.h"

//===========================================
// FIXME, remove more of these?
#include "sysexternal.h"
#include "lib_valve/tier1/tier1.h"
#include "lib_valve/mathlib/mathlib.h"
#include "common.h"
#include "cmd.h"
#include "protocol.h"
#include "render.h"
#include "gl_model.h"


#endif // QUAKEDEF_H
