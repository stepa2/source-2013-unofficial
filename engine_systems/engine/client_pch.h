//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $Workfile:     $
// $Date:         $
// $NoKeywords: $
//=============================================================================//

// main precompiled header for client files

#include "lib_valve/tier0/platform.h"
#include "lib_valve/tier0/basetypes.h"
#include "lib_valve/tier0/vprof.h"
#include "lib_valve/tier0/icommandline.h"
#include "lib_valve/tier1/tier1.h"
#include "lib_valve/tier1/utlbuffer.h"
#include "lib_valve/tier1/utlsymbol.h"
#include "lib_valve/mathlib/mathlib.h"
#include "lib_valve/tier1/fmtstr.h"
#include "lib_valve/tier1/convar.h"

#include "common.h"
#include "qlimits.h"
#include "game/client/iprediction.h"
#include "game/client/icliententitylist.h"

#include "sysexternal.h"
#include "cmd.h"
#include "protocol.h"
#include "render.h"
#include "screen.h"

#include "lib_valve/appframework/ilaunchermgr.h"

#include "gl_shader.h"

#include "cdll_engine_int.h"
#include "shared/client_class.h"
#include "client.h"
#include "cl_main.h"
#include "cl_pred.h"

#include "con_nprint.h"
#include "debugoverlay.h"
#include "demo.h"
#include "host_state.h"
#include "host.h"
#include "filesystem.h"
#include "filesystem_engine.h"
#include "proto_version.h"
#include "sys.h"

//#ifndef SWDS            these are all fine under linux now afaik

#include "vgui_basepanel.h"
#include "vgui_baseui_interface.h"
#include "lib_valve/vgui/IVGui.h"
#include "lib_valve/vgui/IInput.h"
#include "lib_valve/vgui/ILocalize.h"
#include "lib_valve/vgui/IPanel.h"
#include "lib_valve/vgui/IScheme.h"
#include "lib_valve/vgui/ISurface.h"
#include "lib_valve/vgui_controls/Controls.h"
#include "lib_valve/vgui_controls/Label.h"
#include "lib_valve/vgui_controls/TextEntry.h"
#include "lib_valve/vgui_controls/Button.h"
#include "lib_valve/vgui_controls/ComboBox.h"
#include "lib_valve/vgui_controls/CheckButton.h"
#include "lib_valve/vgui_controls/FileOpenDialog.h"
#include "lib_valve/vgui_controls/ScrollBar.h"
#include "lib_valve/vgui_controls/TreeView.h"
//#endif
