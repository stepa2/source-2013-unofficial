//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

#ifndef STUDIO_INTERNAL_H
#define STUDIO_INTERNAL_H
#ifdef _WIN32
#pragma once
#endif

#include "lib_valve/mathlib/vector.h"

struct dworldlight_t;

#endif // STUDIO_INTERNAL_H
