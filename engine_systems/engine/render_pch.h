//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $Workfile:     $
// $Date:         $
// $NoKeywords: $
//=============================================================================//

// main precompiled header for client rendering code

#include "const.h"
#include "lib_valve/tier1/utlsymbol.h"
#include "engine_systems/materialsystem/imesh.h"
#include "engine_systems/materialsystem/imaterial.h"
#include "engine_systems/materialsystem/imaterialvar.h"
#include "engine_systems/materialsystem/imaterialsystemhardwareconfig.h"
#include "engine_systems/materialsystem/materialsystem_config.h"

#include "lib_valve/tier1/convar.h"
#include "gl_model.h"
#include "r_local.h"
#include "gl_shader.h"
#include "cmodel_engine.h"
#include "gl_model_private.h"
#include "gl_cvars.h"
#include "gl_lightmap.h"
#include "gl_matsysiface.h"
#include "gl_rmain.h"
#include "gl_rsurf.h"
#include "gl_matsysiface.h"
#include "render.h"
#include "view.h"
#include "worldsize.h"
#include "sysexternal.h"
