//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: // vcd_sound_check cbase.h
//
//=============================================================================//

#ifndef CBASE_H
#define CBASE_H
#ifdef _WIN32
#pragma once
#endif

#include "lib_valve/tier1/strtools.h"
#include "lib_valve/vstdlib/random.h"
#include "sharedInterface.h"

extern class CSoundEmitterSystemBase *soundemitter;

#endif // CBASE_H
