//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================

#ifndef STDAFX_H
#define STDAFX_H
#ifdef _WIN32
#pragma once
#endif

#include "filesystem.h"
#include "lib_valve/vgui/ILocalize.h"
#include "lib_valve/vgui/IScheme.h"
#include "lib_valve/vgui/ISurface.h"
#include "lib_valve/vgui/ISystem.h"
#include "lib_valve/vgui/IVGui.h"
#include "lib_valve/vgui/Cursor.h"
#include "lib_valve/vgui_controls/Controls.h"
#include "lib_valve/vgui_controls/Panel.h"

#include "lib_valve/p4lib/ip4.h"

#include "vp4dialog.h"

using namespace vgui;

#endif // STDAFX_H
