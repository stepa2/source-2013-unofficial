//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//
#include "DemoPage.h"

#include "lib_valve/VGUI/IVGui.h"
#include "lib_valve/tier1/KeyValues.h"
#include "lib_valve/vgui_controls/ToggleButton.h"
#include "lib_valve/vgui_controls/Tooltip.h"

using namespace vgui;


class TooltipsDemo: public DemoPage
{
	public:
		TooltipsDemo(Panel *parent, const char *name);
		~TooltipsDemo();
	
	private:

};

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
TooltipsDemo::TooltipsDemo(Panel *parent, const char *name) : DemoPage(parent, name)
{
	ToggleButton *pButton = new ToggleButton (this, "RadioDesc5", "");
	pButton->GetTooltip()->SetTooltipFormatToSingleLine();

	LoadControlSettings("Demo/SampleToolTips.res");
}

//-----------------------------------------------------------------------------
// Purpose: Destructor
//-----------------------------------------------------------------------------
TooltipsDemo::~TooltipsDemo()
{
}




Panel* TooltipsDemo_Create(Panel *parent)
{
	return new TooltipsDemo(parent, "TooltipsDemo");
}


