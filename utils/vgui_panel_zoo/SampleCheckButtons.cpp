//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//
#include "DemoPage.h"

#include "lib_valve/VGUI/IVGui.h"
#include "lib_valve/tier1/Keyvalues.h"
#include "lib_valve/vgui_controls/Controls.h"


using namespace vgui;


class SampleCheckButtons: public DemoPage
{
	public:
		SampleCheckButtons(Panel *parent, const char *name);
		~SampleCheckButtons();
	
	private:

};

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
SampleCheckButtons::SampleCheckButtons(Panel *parent, const char *name) : DemoPage(parent, name)
{
	LoadControlSettings("Demo/SampleCheckButtons.res");
}

//-----------------------------------------------------------------------------
// Purpose: Destructor
//-----------------------------------------------------------------------------
SampleCheckButtons::~SampleCheckButtons()
{
}




Panel* SampleCheckButtons_Create(Panel *parent)
{
	return new SampleCheckButtons(parent, "Check buttons");
}


