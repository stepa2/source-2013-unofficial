//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

#include "DemoPage.h"

#include "lib_valve/VGUI/IVGui.h"
#include "lib_valve/vgui_controls/Controls.h"

#include "lib_valve/vgui_controls/Menu.h"
#include "lib_valve/vgui_controls/MenuButton.h"
#include "lib_valve/tier1/Keyvalues.h"

using namespace vgui;


class MenuDemo: public DemoPage
{
	public:
		MenuDemo(Panel *parent, const char *name);
		~MenuDemo();
		void InitMenus();
		
		void OnMaggie();
		
	protected:
		// Menu that opens when button is pressed
		Menu *m_pMenu;

		// Button to trigger the menu
		MenuButton *m_pMenuButton;
		
	private:
		// explain this
		DECLARE_PANELMAP();
				
};