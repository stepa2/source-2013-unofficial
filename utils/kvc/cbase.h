//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================

#ifndef CBASE_H
#define CBASE_H
#ifdef _WIN32
#pragma once
#endif

#include "lib_valve/tier0/basetypes.h"

// This is just a dummy file to make this tool compile
#include "ai_activity.h"
#include "lib_valve/tier1/utlvector.h"

#endif // CBASE_H
