//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

#include "lib_valve/tier1/KeyValues.h"

#include "lib_valve/vgui/ISurface.h"
#include "lib_valve/vgui/IScheme.h"
#include "lib_valve/vgui_controls/cvartogglecheckbutton.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "lib_valve/tier0/memdbgon.h"

using namespace vgui;

vgui::Panel *Create_CvarToggleCheckButton()
{
	return new CvarToggleCheckButton< ConVarRef >( NULL, NULL );
}

DECLARE_BUILD_FACTORY_CUSTOM_ALIAS( CvarToggleCheckButton<ConVarRef>, CvarToggleCheckButton, Create_CvarToggleCheckButton );

