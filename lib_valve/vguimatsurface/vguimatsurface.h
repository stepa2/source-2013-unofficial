//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Implementation of the VGUI ISurface interface using the 
// material system to implement it
//
// $Revision: $
// $NoKeywords: $
//===========================================================================//

#ifndef VGUIMATSURFACE_H
#define VGUIMATSURFACE_H

#include "lib_valve/vgui/VGUI.h"
#include "lib_valve/tier3/tier3.h"


namespace vgui
{
	class IInputInternal; 
}

//-----------------------------------------------------------------------------
// Globals...
//-----------------------------------------------------------------------------
extern vgui::IInputInternal *g_pIInput;


#endif // VGUIMATSURFACE_H


