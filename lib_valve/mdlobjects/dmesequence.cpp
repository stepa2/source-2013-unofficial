//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Dme representation of QC: $sequence
//
//===========================================================================//


#include "lib_valve/datamodel/dmelementfactoryhelper.h"
#include "lib_valve/movieobjects/dmeanimationlist.h"
#include "lib_valve/movieobjects/dmechannel.h"
#include "lib_valve/movieobjects/dmedag.h"
#include "lib_valve/mdlobjects/dmesequence.h"


// memdbgon must be the last include file in a .cpp file!!!
#include "lib_valve/tier0/memdbgon.h"


//-----------------------------------------------------------------------------
// Expose this class to the scene database 
//-----------------------------------------------------------------------------
IMPLEMENT_ELEMENT_FACTORY( DmeSequence, CDmeSequence );


//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
void CDmeSequence::OnConstruction()
{
	m_Skeleton.Init( this, "skeleton" );
	m_AnimationList.Init( this, "animationList" );
}


//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
void CDmeSequence::OnDestruction()
{
}