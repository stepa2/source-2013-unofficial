//========= Copyright Valve Corporation, All rights reserved. ============//
//
// A list of DmeBoneWeight elements, replacing QC's $WeightList
//
//===========================================================================//


#include "lib_valve/datamodel/dmelementfactoryhelper.h"
#include "lib_valve/mdlobjects/dmebonemask.h"
#include "lib_valve/mdlobjects/dmebonemasklist.h"


// memdbgon must be the last include file in a .cpp file!!!
#include "lib_valve/tier0/memdbgon.h"


//-----------------------------------------------------------------------------
// Expose this class to the scene database 
//-----------------------------------------------------------------------------
IMPLEMENT_ELEMENT_FACTORY( DmeBoneMaskList, CDmeBoneMaskList );


//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
void CDmeBoneMaskList::OnConstruction()
{
	m_BoneMasks.Init( this, "boneMasks" );
}


//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
void CDmeBoneMaskList::OnDestruction()
{
}