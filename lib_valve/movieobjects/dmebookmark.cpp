//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================
#include "lib_valve/movieobjects/dmebookmark.h"
#include "lib_valve/tier0/dbg.h"
#include "lib_valve/datamodel/dmelementfactoryhelper.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "lib_valve/tier0/memdbgon.h"


//-----------------------------------------------------------------------------
// Class factory 
//-----------------------------------------------------------------------------
IMPLEMENT_ELEMENT_FACTORY( DmeBookmark, CDmeBookmark );


//-----------------------------------------------------------------------------
// Constructor, destructor 
//-----------------------------------------------------------------------------
void CDmeBookmark::OnConstruction()
{
	m_Time.InitAndSet( this, "time", 0 );
	m_Duration.InitAndSet( this, "duration", 0 );
	m_Note.Init( this, "note" );
}

void CDmeBookmark::OnDestruction()
{
}
