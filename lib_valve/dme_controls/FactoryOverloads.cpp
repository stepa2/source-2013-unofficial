//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//

#include "lib_valve/tier1/KeyValues.h"
#include "lib_valve/dme_controls/ElementPropertiesTree.h"
#include "lib_valve/datamodel/dmelement.h"

#include "lib_valve/vgui_controls/TextEntry.h"
#include "lib_valve/vgui_controls/ComboBox.h"
#include "lib_valve/vgui_controls/Button.h"
#include "lib_valve/vgui_controls/PanelListPanel.h"

#include "FactoryOverloads.h"

void CFactoryOverloads::AddOverload( 
	char const *attributeName, 
	IAttributeWidgetFactory *newFactory,
	IAttributeElementChoiceList *newChoiceList )
{
	Assert( attributeName );
	Assert( newFactory || newChoiceList );

	if ( !newFactory )
	{
		return;
	}

	Entry_t e;
	e.factory = newFactory;
	e.choices = newChoiceList;

	m_Overloads.Insert( attributeName, e );
}

int CFactoryOverloads::Count()
{
	return m_Overloads.Count();
}

char const *CFactoryOverloads::Name( int index )
{
	return m_Overloads.GetElementName( index );
}

IAttributeWidgetFactory *CFactoryOverloads::Factory( int index )
{
	return m_Overloads[ index ].factory;
}

IAttributeElementChoiceList *CFactoryOverloads::ChoiceList( int index )
{
	return m_Overloads[ index ].choices;
}