//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================

#include "lib_valve/dme_controls/dmemdlpanel.h"
#include "lib_valve/dme_controls/dmecontrols.h"
#include "lib_valve/dme_controls/dmepanel.h"
#include "lib_valve/movieobjects/dmemdl.h"
#include "lib_valve/movieobjects/dmemdlmakefile.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "lib_valve/tier0/memdbgon.h"

using namespace vgui;


IMPLEMENT_DMEPANEL_FACTORY( CDmeMDLPanel, DmeMDLMakefile, "DmeMakeFileOutputPreview", "MDL MakeFile Output Preview", false );


//-----------------------------------------------------------------------------
// Constructor, destructor
//-----------------------------------------------------------------------------
CDmeMDLPanel::CDmeMDLPanel( vgui::Panel *pParent, const char *pName ) : BaseClass( pParent, pName )
{
}

CDmeMDLPanel::~CDmeMDLPanel()
{
}


//-----------------------------------------------------------------------------
// DMEPanel..
//-----------------------------------------------------------------------------
void CDmeMDLPanel::SetDmeElement( CDmeMDLMakefile *pMDLMakefile )
{
	if ( pMDLMakefile != NULL )
	{
		CDmeMDL *pMDL = CastElement< CDmeMDL >( pMDLMakefile->GetOutputElement( true ) );
		if ( pMDL )
		{
			SetMDL( pMDL->GetMDL() );
		}
	}
}
