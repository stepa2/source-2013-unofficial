//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

#ifndef CBASE_H
#define CBASE_H
#ifdef _WIN32
#pragma once
#endif

#include "hlfaceposer.h"
#include "lib_valve/tier1/strtools.h"
#include "lib_valve/vstdlib/random.h"
#include "sharedInterface.h"

extern class ISoundEmitterSystemBase *soundemitter;

#endif // CBASE_H
